# popsical-flutter

An assessment for a senior mobile software engineer job with popsical.

## Getting Started

The app comes with 3 environment 

Use any of the below command to run the project base on the environment you want.


flutter run --flavor dev -t lib/main_dev.dart

or 

flutter run --flavor prod -t lib/main_prod.dart

or 

flutter run --flavor uat -t lib/main_uat.dart

