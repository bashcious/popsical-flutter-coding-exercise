import 'package:flutter/foundation.dart';

class Genre {
  String name;
  bool selected;

  Genre({@required this.name, bool this.selected = false});
}
