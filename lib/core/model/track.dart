class TrackResponse {
  TrackResponse({this.tracks, this.meta});
  List<Track> tracks;
  Meta meta;
  TrackResponse.fromJson(Map<String, dynamic> json) {
    meta = json['meta'] != null ? Meta.fromJson(json['meta']) : null;
    tracks = (json['tracks'] as List)
        ?.map(
            (e) => e == null ? null : Track.fromJson(e as Map<String, dynamic>))
        ?.toList();
  }
}

class Track {
  int id;
  String title;
  String releaseDate;
  Image images;
  List<TrackArtist> trackArtists;

  Track({this.id, this.title, this.releaseDate});

  Track.fromJson(Map<String, dynamic> map) {
    id = map['id'] as int;
    title = map['title'] as String;
    releaseDate = map['release_date'] as String;
    images = map['images'] != null ? Image.fromJson(map['images']) : null;
    trackArtists = (map['track_artists'] as List)
        ?.map((e) =>
            e == null ? null : TrackArtist.fromJson(e as Map<String, dynamic>))
        ?.toList();
  }
}

class Image {
  Poster poster;
  Image({
    this.poster,
  });
  Image.fromJson(Map<String, dynamic> map) {
    poster = map['poster'] != null ? Poster.fromJson(map['poster']) : null;
  }
}

class Poster {
  String url;
  Poster({
    this.url,
  });
  Poster.fromJson(Map<String, dynamic> map) {
    url = map['url'] as String;
  }
}

class Meta {
  int currentPage;
  int numPages;

  Meta({
    this.currentPage,
    this.numPages,
  });

  Meta.fromJson(Map<String, dynamic> map) {
    currentPage = map['current_page'] as int;
    numPages = map['num_pages'] as int;
  }
}

class TrackArtist {
  Artist artist;
  String type;
  TrackArtist({this.artist, this.type});
  TrackArtist.fromJson(Map<String, dynamic> map) {
    artist = map['artist'] != null ? Artist.fromJson(map['artist']) : null;
    type = map['type'] as String;
  }
}

class Artist {
  String name;
  Artist({
    this.name,
  });
  Artist.fromJson(Map<String, dynamic> map) {
    name = map['name'] as String;
  }
}
