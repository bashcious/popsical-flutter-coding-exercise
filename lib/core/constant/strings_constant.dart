class ConstantStrings {
  ConstantStrings._();

  static String appName = 'POPSICAL';
  static String kNoInternetFullMsg =
      'No internet Connection!! Please connect to internet and try again';
  static String kLoadingWithDot = 'Loading...';
  static String kLandingTitle = 'Flutter';
  static String kCancel = 'Cancel';
  static String kOk = 'Ok';

  static String kGenres = 'GENRES';
  static String kTracks = 'TRACKS';
  static String kHome = 'Home';
}
