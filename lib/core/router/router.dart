import 'package:base_flutter/ui/view/landing/genre/genre_view.dart';
import 'package:base_flutter/ui/view/landing/landing.dart';
import 'package:flutter/material.dart';

class Router {
  static const String landing = '/landingRoute';
  static const String genreRoute = '/genreRoute';

  static MaterialPageRoute _pageRoute(Widget page) {
    return MaterialPageRoute(builder: (_) => page);
  }

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case landing:
        return _pageRoute(LandingScreen());
      case genreRoute:
        return _pageRoute(GenreScreen(
          arguments: settings.arguments,
        ));
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                      child: Text('No route defined for ${settings?.name}')),
                ));
    }
  }
}
