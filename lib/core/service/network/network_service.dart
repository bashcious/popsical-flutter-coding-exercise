import 'dart:convert';
import 'dart:io';
import 'package:base_flutter/core/constant/strings_constant.dart';
import 'package:base_flutter/core/model/track.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import '../flavor_manager.dart';

abstract class NetworkService {
  Future<TrackResponse> getTrackList({@required int page});
  Future<TrackResponse> getGenresList(
      {@required int page, @required String genre});
}

class NetworkServiceImpl implements NetworkService {
  final String _baseUrl = FlavorManager.instance.settings.baseUrl;
  String _trackListUrl({int page}) => '/songs.json?page=${page ?? 1}';
  String _genresListUrl({int page, String genre}) =>
      '/songs.json?genres=$genre&page=${page ?? 1}';

  final Client _client = Client();

  Map<String, String> get headers {
    return {HttpHeaders.contentTypeHeader: 'application/json'};
  }

  @override
  Future<TrackResponse> getTrackList({int page}) async {
    try {
      final url = '$_baseUrl${_trackListUrl(page: page)}';
      final response = await _client.get(url, headers: headers);
      final Map<String, dynamic> jsonResponse = json.decode(response.body);
      return TrackResponse.fromJson(jsonResponse);
    } on SocketException catch (_) {
      return Future.error(ConstantStrings.kNoInternetFullMsg);
    } catch (e) {
      return Future.error(e.toString());
    }
  }

  @override
  Future<TrackResponse> getGenresList({int page, String genre}) async {
    try {
      final url = '$_baseUrl${_genresListUrl(page: page, genre: genre)}';
      final response = await _client.get(url, headers: headers);
      final Map<String, dynamic> jsonResponse = json.decode(response.body);
      return TrackResponse.fromJson(jsonResponse);
    } on SocketException catch (_) {
      return Future.error(ConstantStrings.kNoInternetFullMsg);
    } catch (e) {
      return Future.error(e.toString());
    }
  }
}
