import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class NavigationService {
  final GlobalKey<NavigatorState> _navigationKey = GlobalKey<NavigatorState>();

  GlobalKey<NavigatorState> get navigationKey => _navigationKey;

  void pop() {
    return _navigationKey.currentState.pop();
  }

  Future<dynamic> navigateTo(String routeName, {dynamic arguments}) {
    return _navigationKey.currentState
        .pushNamed(routeName, arguments: arguments);
  }

  Future<dynamic> pushAndRemoveUntil(String pushNamed,
      {String removeUntilNamed, Object arguments}) async {
    if (removeUntilNamed != null) {
      return _navigationKey.currentState.pushNamedAndRemoveUntil(
        pushNamed,
        ModalRoute.withName(removeUntilNamed),
        arguments: arguments,
      );
    } else {
      return _navigationKey.currentState.pushNamedAndRemoveUntil(
        pushNamed,
        (route) => false,
        arguments: arguments,
      );
    }
  }

  void popUntilFirstRoute() {
    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      _navigationKey.currentState.popUntil((route) => route.isFirst);
    });
  }
}
