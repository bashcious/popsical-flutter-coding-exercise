import 'package:base_flutter/core/model/track.dart';
import 'package:base_flutter/core/service/network/network_service.dart';
import 'package:flutter/material.dart';
import 'locator/locator.dart';

abstract class Service {
  Future<TrackResponse> getTrackList({@required int page});
  Future<TrackResponse> getGenresList(
      {@required int page, @required String genre});
}

class ServiceImpl implements Service {
  final NetworkService _networkService = locator<NetworkServiceImpl>();

  @override
  Future<TrackResponse> getTrackList({int page}) async {
    return await _networkService.getTrackList(page: page);
  }

  @override
  Future<TrackResponse> getGenresList({int page, String genre}) async {
    return await _networkService.getGenresList(page: page, genre: genre);
  }
}
