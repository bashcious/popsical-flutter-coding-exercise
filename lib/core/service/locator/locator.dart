import 'package:base_flutter/core/service/navigator/navigation_service.dart';
import 'package:base_flutter/core/service/network/network_service.dart';
import 'package:base_flutter/core/service/service.dart';
import 'package:base_flutter/core/viewmodel/genre_view_model.dart';
import 'package:base_flutter/core/viewmodel/startup_view_model.dart';
import 'package:base_flutter/core/viewmodel/track_view_model.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton(() => NetworkServiceImpl());
  locator.registerLazySingleton(() => ServiceImpl());
  locator.registerLazySingleton(() => NavigationService());

  locator.registerFactory(() => TrackViewModel());
  locator.registerFactory(() => StartUpViewModel());
  locator.registerFactory(() => GenreViewModel());
}
