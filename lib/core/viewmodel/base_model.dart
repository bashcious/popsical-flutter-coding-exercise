import 'package:flutter/material.dart';
import '../enum/view_state.dart';

class BaseModel extends ChangeNotifier {
  ViewState _state;
  ViewState get state => _state;

  void setState(ViewState newState) {
    _state = newState;
    notifyListeners();
  }
}
