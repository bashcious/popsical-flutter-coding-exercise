import 'package:base_flutter/core/argument/argument.dart';
import 'package:base_flutter/core/enum/view_state.dart';
import 'package:base_flutter/core/model/genre.dart';
import 'package:base_flutter/core/model/track.dart';
import 'package:base_flutter/core/router/router.dart';
import 'package:base_flutter/core/service/locator/locator.dart';
import 'package:base_flutter/core/service/navigator/navigation_service.dart';
import 'package:base_flutter/core/service/service.dart';
import 'base_model.dart';

class TrackViewModel extends BaseModel {
  final Service _service = locator<ServiceImpl>();
  TrackResponse _trackResponse;
  TrackResponse get trackResponse => _trackResponse;
  dynamic _error;
  dynamic get error => _error;
  int _currentPage = 1;
  int get currentPage => _currentPage;
  final NavigationService _navigationService = locator<NavigationService>();
  set currentPage(int value) {
    _currentPage = value;
  }

  List<Genre> genresList = [
    Genre(
      name: 'pop',
      selected: false,
    ),
    Genre(
      name: 'rock',
      selected: false,
    ),
    Genre(
      name: 'hip hop',
      selected: false,
    ),
    Genre(
      name: 'country',
      selected: false,
    ),
  ];
  void selectGenre(Genre genre) {
    genresList
        .map((model) =>
            {model.selected = model.name == genre.name ? true : false})
        .toList();
    notifyListeners();
    _navigationService.navigateTo(Router.genreRoute,
        arguments: SelectGenreScreenArgument(
          genre: genre,
        ));
  }

  void _setErrorMsg(String error) {
    _error = error;
  }

  Future getTrackList({bool isShowLoader, int page}) async {
    if (isShowLoader) {
      setState(ViewState.busy);
    } else {
      setState(ViewState.loadMoreBusy);
    }
    try {
      final response = await _service.getTrackList(page: page);
      if (isShowLoader) {
        _trackResponse = response;
      } else {
        _trackResponse?.tracks?.addAll(response?.tracks);
      }
      _currentPage = response?.meta?.currentPage;
      setState(ViewState.idle);
    } catch (error) {
      _setErrorMsg(error.toString());
      setState(ViewState.error);
    }
  }
}
