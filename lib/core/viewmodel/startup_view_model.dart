import 'package:base_flutter/core/router/router.dart';
import 'package:base_flutter/core/service/locator/locator.dart';
import 'package:base_flutter/core/service/navigator/navigation_service.dart';

import 'base_model.dart';

class StartUpViewModel extends BaseModel {
  final NavigationService _navigationService = locator<NavigationService>();

  Future handleStartUpLogic() async {
    Future.delayed(const Duration(seconds: 2), () {
      _navigationService.pushAndRemoveUntil(Router.landing);
      ;
    });
  }
}
