import 'package:base_flutter/core/enum/view_state.dart';
import 'package:base_flutter/core/model/track.dart';
import 'package:base_flutter/core/service/locator/locator.dart';
import 'package:base_flutter/core/service/service.dart';
import 'base_model.dart';

class GenreViewModel extends BaseModel {
  final Service _service = locator<ServiceImpl>();
  TrackResponse _genresResponse;
  TrackResponse get genresResponse => _genresResponse;
  dynamic _error;
  dynamic get error => _error;
  int _currentPage = 1;
  int get currentPage => _currentPage;

  set currentPage(int value) {
    _currentPage = value;
  }

  void _setErrorMsg(String error) {
    _error = error;
  }

  Future getGenresList({bool isShowLoader, int page, String genre}) async {
    if (isShowLoader) {
      setState(ViewState.busy);
    } else {
      setState(ViewState.loadMoreBusy);
    }
    try {
      final response = await _service.getGenresList(page: page, genre: genre);
      if (isShowLoader) {
        _genresResponse = response;
      } else {
        _genresResponse?.tracks?.addAll(response?.tracks);
      }
      _currentPage = response?.meta?.currentPage;
      setState(ViewState.idle);
    } catch (error) {
      _setErrorMsg(error.toString());
      setState(ViewState.error);
    }
  }
}
