import 'package:base_flutter/core/model/genre.dart';
import 'package:flutter/foundation.dart';

class SelectGenreScreenArgument {
  Genre genre;

  SelectGenreScreenArgument({@required this.genre});
}
