import 'package:base_flutter/ui/view/splash/splash_screen.dart';
import 'package:flutter/material.dart';
import 'core/constant/strings_constant.dart';
import 'core/router/router.dart';
import 'core/service/locator/locator.dart';
import 'core/service/navigator/navigation_service.dart';

class PopsicalApp extends StatelessWidget {
  const PopsicalApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: locator<NavigationService>().navigationKey,
      debugShowCheckedModeBanner: false,
      title: ConstantStrings.appName,
      onGenerateRoute: Router.generateRoute,
      home: SplashScreen(),
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
    );
  }
}
