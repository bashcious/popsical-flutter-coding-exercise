import 'package:flutter/material.dart';

class UbuntuStyle {
  UbuntuStyle._();

  static TextStyle defaultStyle = const TextStyle(fontFamily: 'Ubuntu');

  static TextStyle display = defaultStyle.copyWith(
      fontSize: 48, fontWeight: FontWeight.w700, height: 65);

  static TextStyle h1 = defaultStyle.copyWith(
      fontSize: 32, fontWeight: FontWeight.w500, height: 43);

  static TextStyle h2 = defaultStyle.copyWith(
      fontSize: 24, fontWeight: FontWeight.w500, height: 32);

  static TextStyle h3 = defaultStyle.copyWith(
      fontSize: 20, fontWeight: FontWeight.w500, height: 27);

  static TextStyle h4 = defaultStyle.copyWith(
      fontSize: 18, fontWeight: FontWeight.w500, height: 25);

  static TextStyle button1 = defaultStyle.copyWith(
      fontSize: 14, fontWeight: FontWeight.w600, height: 18, letterSpacing: 1);

  static TextStyle button2 = defaultStyle.copyWith(
      fontSize: 14, fontWeight: FontWeight.w400, letterSpacing: 0.5);
}
