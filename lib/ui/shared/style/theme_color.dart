import 'package:flutter/material.dart';

class ThemeColor {
  ThemeColor._();

  static MaterialColor engine = const MaterialColor(0xFF1E88E5, <int, Color>{
    300: Color(0xFF64B5F6),
    800: Color(0xFF1565C0),
    900: Color(0xFF0D47A1)
  });

  static MaterialColor nitro = const MaterialColor(0xFFE57B1E, <int, Color>{
    300: Color(0xFFF2BC8D),
    800: Color(0xFFCE6D18),
    900: Color(0xFFA05512)
  });

  static MaterialColor black = const MaterialColor(0xFF231F20, <int, Color>{
    80: Color.fromRGBO(35, 31, 32, 0.8),
    56: Color.fromRGBO(35, 31, 32, 0.56),
    24: Color.fromRGBO(35, 31, 32, 0.24),
    08: Color.fromRGBO(35, 31, 32, 0.08)
  });

  static Color background = const Color(0xFFF5F5F5);

  static Color white = const Color(0xFFFFFFFF);

  static Color brand01 = const Color(0xFFFFCC32);

  static Color brand03 = const Color(0xFF2D7BB8);

  static Color supportSuccess = const Color(0xFF2AAF78);

  static Color supportError = const Color(0xFFF4511E);
}
