import 'package:base_flutter/ui/shared/style/theme_color.dart';
import 'package:flutter/material.dart';

final ThemeData defaultThemeData = ThemeData(
  primaryColor: ThemeColor.brand01,
  fontFamily: 'Roboto',
  accentColor: ThemeColor.brand03,
  scaffoldBackgroundColor: ThemeColor.background
);