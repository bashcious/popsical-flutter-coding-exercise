import 'package:flutter/material.dart';

const Color dialogButtonCancelColor = Color(0xffCEB6B6);
const Color kMainColor = Colors.cyan;

const TextStyle dialogCancelTextStyle = TextStyle(
  color: dialogButtonCancelColor,
  fontSize: 15,
);

const TextStyle dialogDoneTextStyle = TextStyle(
  color: Colors.red,
  fontSize: 15,
);

const TextStyle appBarTitleTextStyle = TextStyle(
  color: Colors.white,
  fontSize: 20,
);
