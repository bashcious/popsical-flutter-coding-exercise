import 'package:flutter/material.dart';

class ProgressDialog extends StatelessWidget {
  final String loadingMsg;
  const ProgressDialog({@required this.loadingMsg});
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          const CircularProgressIndicator(
            backgroundColor: Colors.blue,
            valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
          ),
          Container(
            height: 32.0,
          ),
          Text(
            loadingMsg,
            style: const TextStyle(color: Colors.blue),
          )
        ],
      ),
    );
  }
}
