import 'package:base_flutter/ui/shared/style/style.dart';
import 'package:flutter/material.dart';

AppBar getAppBar({
  @required String title,
  List<Widget> actions,
}) {
  return AppBar(
    title: Text(
      title,
      style: appBarTitleTextStyle,
    ),
    backgroundColor: kMainColor,
    elevation: 0.0,
    centerTitle: true,
    iconTheme: const IconThemeData(
      color: Colors.white,
    ),
    actions: actions ?? [],
  );
}
