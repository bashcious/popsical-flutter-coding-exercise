import 'package:base_flutter/ui/shared/style/roboto_style.dart';
import 'package:flutter/material.dart';

class TrackCell extends StatelessWidget {
  final dynamic track;

  const TrackCell(
    this.track,
  );
  @override
  Widget build(BuildContext context) {
    final artistName = StringBuffer();
    if (track.trackArtists.length != null && track.trackArtists.isNotEmpty) {
      for (var i = 0; i < track.trackArtists.length; i++) {
        if (i != track.trackArtists.length - 1) {
          artistName.write('${track.trackArtists[i].artist.name}, ');
        } else {
          artistName.write('${track.trackArtists[i].artist.name}');
        }
      }
    }
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Card(
          margin: const EdgeInsets.all(0.0),
          clipBehavior: Clip.antiAlias,
          child: track?.images?.poster?.url != null &&
                  track.images.poster.url.isNotEmpty
              ? Container(
                  height: 30,
                  width: 30,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        colorFilter: ColorFilter.mode(
                            Colors.black.withOpacity(0.4), BlendMode.darken),
                        image: NetworkImage(track?.images?.poster?.url)),
                  ))
              : Container(
                  color: Colors.grey,
                ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                track.title,
                style: RobotoStyle.caption2,
              ),
              if (artistName.isNotEmpty)
                Text(
                  artistName.toString(),
                  style: RobotoStyle.body2.copyWith(fontSize: 12),
                )
            ],
          ),
        ),
      ],
    );
  }
}
