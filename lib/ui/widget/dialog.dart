import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';

class Dialogs {
  static Widget showErrorDialog(
    BuildContext context, {
    @required String errorText,
    @required String cancelButtonText,
    @required String doneButtonText,
    @required TextStyle dialogCancelTextStyle,
    @required TextStyle dialogDoneTextStyle,
    Function onDone,
    Function onCancel,
  }) {
    final todayDate = DateTime.now();

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Center(
        child: Card(
          elevation: 4,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(formatDate(todayDate, [dd, ' ', M, ' ', yyyy])),
                    const Spacer(),
                    Text(formatDate(todayDate, [HH, ':', nn]))
                  ],
                ),
              ),
              Container(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Text(
                  errorText,
                  style: const TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                  ),
                ),
              ),
              Container(
                height: 32,
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: FlatButton(
                      onPressed: () {
                        onCancel();
                      },
                      child: Text(
                        cancelButtonText ?? 'Cancel',
                        style: dialogCancelTextStyle,
                      ),
                    ),
                  ),
                  Expanded(
                    child: FlatButton(
                      onPressed: () {
                        onDone();
                      },
                      child: Text(
                        doneButtonText ?? 'OK',
                        style: dialogDoneTextStyle,
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
