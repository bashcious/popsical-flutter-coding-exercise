import 'package:base_flutter/core/constant/strings_constant.dart';
import 'package:base_flutter/core/enum/view_state.dart';
import 'package:base_flutter/core/model/genre.dart';
import 'package:base_flutter/core/service/locator/locator.dart';
import 'package:base_flutter/core/service/navigator/navigation_service.dart';
import 'package:base_flutter/core/viewmodel/track_view_model.dart';
import 'package:base_flutter/ui/shared/style/roboto_style.dart';
import 'package:base_flutter/ui/shared/style/style.dart';
import 'package:base_flutter/ui/shared/style/theme_color.dart';
import 'package:base_flutter/ui/widget/dialog.dart';
import 'package:base_flutter/ui/widget/progress_dialog.dart';
import 'package:base_flutter/ui/widget/track_cell.dart';
import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_sticky_header/flutter_sticky_header.dart';
import 'package:provider/provider.dart';
import '../../base_view.dart';

class TrackScreen extends StatelessWidget {
  final NavigationService _navigationService = locator<NavigationService>();
  @override
  Widget build(BuildContext context) {
    return BaseView<TrackViewModel>(
      viewModel: Provider.of<TrackViewModel>(context),
      onModelReady: (model) => Future.microtask(
        () => model.getTrackList(isShowLoader: true, page: 1),
      ),
      builder: (context, model, child) => Container(
        child: _getBodyUi(context: context, model: model),
      ),
    );
  }

  Widget _getBodyUi({TrackViewModel model, BuildContext context}) {
    return Selector<TrackViewModel, ViewState>(
      selector: (_, store) => model?.state,
      builder: (context, state, __) {
        switch (model.state) {
          case ViewState.busy:
            return ProgressDialog(
              loadingMsg: ConstantStrings.kLoadingWithDot,
            );
            break;
          case ViewState.idle:
          case ViewState.loadMoreBusy:
            return contentBody(model);
            break;
          case ViewState.error:
            return _handleErrorData(model: model, context: context);
            break;
          default:
            return Container();
        }
      },
    );
  }

  Widget _handleErrorData({TrackViewModel model, BuildContext context}) {
    Future(() {
      showDialog(
          context: context,
          builder: (context) => Dialogs.showErrorDialog(context,
              errorText: model.error.toString(),
              cancelButtonText: ConstantStrings.kCancel,
              doneButtonText: ConstantStrings.kOk,
              dialogCancelTextStyle: dialogCancelTextStyle,
              dialogDoneTextStyle: dialogDoneTextStyle,
              onDone: () => _navigationService.pop(),
              onCancel: () => _navigationService.pop()));
    });
    return ListView(
      children: const <Widget>[],
    );
  }

  Widget contentBody(TrackViewModel viewModel) {
    return Consumer<TrackViewModel>(
      builder: (context, viewModel, _) {
        return NotificationListener<ScrollNotification>(
          onNotification: (ScrollNotification scrollInfo) {
            if (viewModel.trackResponse.tracks != null &&
                viewModel.currentPage <
                    viewModel?.trackResponse?.meta?.numPages &&
                (viewModel.state != ViewState.loadMoreBusy &&
                    scrollInfo.metrics.pixels ==
                        scrollInfo.metrics.maxScrollExtent)) {
              viewModel.getTrackList(
                  isShowLoader: false, page: viewModel.currentPage + 1);
            }
            return false;
          },
          child: CustomScrollView(
            slivers: <Widget>[
              getGenre(viewModel, context),
              getTrack(viewModel, context)
            ],
          ),
        );
      },
    );
  }

  Widget getGenre(TrackViewModel viewModel, BuildContext context) {
    final List<Genre> genresList = viewModel.genresList;
    return SliverStickyHeader(
        header: Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
          color: kMainColor,
          child: Text(
            ConstantStrings.kGenres,
            style: RobotoStyle.caption2.copyWith(color: ThemeColor.white),
          ),
        ),
        sliver: SliverList(
          delegate: SliverChildBuilderDelegate(
            (context, i) => Container(
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.all(16),
              child: Wrap(
                runSpacing: 12,
                spacing: 12,
                children: <Widget>[
                  for (Genre genre in genresList)
                    OutlineButton(
                      padding: const EdgeInsets.all(8),
                      shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(3))),
                      borderSide: BorderSide(
                          color:
                              genre.selected ? ThemeColor.engine : Colors.grey),
                      onPressed: () => viewModel.selectGenre(genre),
                      child: Container(
                        height: 56,
                        width: 100,
                        child: Center(
                          child: SizedBox(
                            width: 80,
                            height: 40,
                            child: Center(
                              child: Text(StringUtils.capitalize(genre.name)),
                            ),
                          ),
                        ),
                      ),
                    ),
                ],
              ),
            ),
            childCount: 1,
          ),
        ));
  }

  Widget getTrack(TrackViewModel viewModel, BuildContext context) {
    return SliverStickyHeader(
        header: Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
          color: kMainColor,
          child: Text(
            ConstantStrings.kTracks,
            style: RobotoStyle.caption2.copyWith(color: ThemeColor.white),
          ),
        ),
        sliver: SliverList(
          delegate: SliverChildBuilderDelegate(
            (context, index) {
              final track = viewModel?.trackResponse?.tracks[index];
              return FlatButton(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                onPressed: () {},
                child: TrackCell(track),
              );
            },
            childCount: viewModel.trackResponse.tracks.length ?? 0,
          ),
        ));
  }
}
