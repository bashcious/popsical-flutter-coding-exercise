import 'package:base_flutter/core/argument/argument.dart';
import 'package:base_flutter/core/constant/strings_constant.dart';
import 'package:base_flutter/core/enum/view_state.dart';
import 'package:base_flutter/core/service/locator/locator.dart';
import 'package:base_flutter/core/service/navigator/navigation_service.dart';
import 'package:base_flutter/core/viewmodel/genre_view_model.dart';
import 'package:base_flutter/ui/shared/style/style.dart';
import 'package:base_flutter/ui/widget/app_bar.dart';
import 'package:base_flutter/ui/widget/dialog.dart';
import 'package:base_flutter/ui/widget/progress_dialog.dart';
import 'package:base_flutter/ui/widget/track_cell.dart';
import 'package:basic_utils/basic_utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../base_view.dart';

class GenreScreen extends StatelessWidget {
  final SelectGenreScreenArgument arguments;
  GenreScreen({@required this.arguments});
  final NavigationService _navigationService = locator<NavigationService>();

  @override
  Widget build(BuildContext context) {
    return BaseView<GenreViewModel>(
      onModelReady: (model) => model.getGenresList(
          isShowLoader: true, page: 1, genre: arguments?.genre?.name),
      builder: (context, model, child) => Scaffold(
        appBar: getAppBar(
            title: StringUtils.capitalize(
                arguments?.genre?.name ?? ConstantStrings.kGenres)),
        body: Container(
          color: Colors.transparent,
          child: _getBodyUi(context: context, model: model),
        ),
        floatingActionButton: loadMoreLoader(model: model),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      ),
    );
  }

  Widget _getBodyUi({GenreViewModel model, BuildContext context}) {
    return Selector<GenreViewModel, ViewState>(
      selector: (_, store) => model?.state,
      builder: (context, state, __) {
        switch (model.state) {
          case ViewState.busy:
            return ProgressDialog(
              loadingMsg: ConstantStrings.kLoadingWithDot,
            );
            break;
          case ViewState.idle:
          case ViewState.loadMoreBusy:
            return _buildContent(context: context, model: model);
            break;
          case ViewState.error:
            return _handleErrorData(model: model, context: context);
            break;
          default:
            return _buildContent(context: context, model: model);
        }
      },
    );
  }

  Widget _handleErrorData({GenreViewModel model, BuildContext context}) {
    Future(() {
      showDialog(
          context: context,
          builder: (context) => Dialogs.showErrorDialog(context,
              errorText: model.error.toString(),
              cancelButtonText: ConstantStrings.kCancel,
              doneButtonText: ConstantStrings.kOk,
              dialogCancelTextStyle: dialogCancelTextStyle,
              dialogDoneTextStyle: dialogDoneTextStyle,
              onDone: () => _navigationService.pop(),
              onCancel: () => _navigationService.pop()));
    });
    return Container();
  }

  Widget _buildContent({BuildContext context, GenreViewModel model}) {
    if (model?.genresResponse != null) {
      return NotificationListener<ScrollNotification>(
        onNotification: (ScrollNotification scrollInfo) {
          if (model.genresResponse.tracks != null &&
              model.currentPage < model?.genresResponse?.meta?.numPages &&
              (model.state != ViewState.loadMoreBusy &&
                  scrollInfo.metrics.pixels ==
                      scrollInfo.metrics.maxScrollExtent)) {
            model.getGenresList(
                isShowLoader: false,
                page: model.currentPage + 1,
                genre: arguments?.genre?.name);
          }
          return false;
        },
        child: ListView.builder(
          padding: const EdgeInsets.only(top: 20),
          itemCount: model?.genresResponse?.tracks?.length ?? 0,
          itemBuilder: (context, index) {
            final track = model?.genresResponse?.tracks[index];
            return FlatButton(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              onPressed: () {},
              child: TrackCell(track),
            );
            ;
          },
        ),
      );
    } else {
      return Container();
    }
  }

  Widget loadMoreLoader({
    GenreViewModel model,
  }) {
    return Selector<GenreViewModel, ViewState>(
      selector: (_, store) => model?.state,
      builder: (context, state, __) {
        return Visibility(
            visible: state == ViewState.loadMoreBusy ? true : false,
            child: const CircularProgressIndicator());
      },
    );
  }
}
