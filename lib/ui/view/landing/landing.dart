import 'package:base_flutter/core/constant/strings_constant.dart';
import 'package:base_flutter/core/enum/view_state.dart';
import 'package:base_flutter/core/viewmodel/track_view_model.dart';
import 'package:base_flutter/ui/shared/style/roboto_style.dart';
import 'package:base_flutter/ui/shared/style/style.dart';
import 'package:base_flutter/ui/view/base_view.dart';
import 'package:base_flutter/ui/view/landing/track/track_view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LandingScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return BaseView<TrackViewModel>(
      builder: (context, viewModel, _) {
        return Scaffold(
          backgroundColor: Colors.white,
          body: SafeArea(
            child: NestedScrollView(
                physics: const ClampingScrollPhysics(),
                headerSliverBuilder: (context, boxIsScrolled) {
                  return <Widget>[
                    SliverOverlapAbsorber(
                      handle: NestedScrollView.sliverOverlapAbsorberHandleFor(
                          context),
                      sliver: SliverSafeArea(
                        top: false,
                        sliver: SliverAppBar(
                          elevation: 0,
                          title:  Text(
                            ConstantStrings.kHome,
                            style: TextStyle(color: Colors.black),
                          ),
                          centerTitle: false,
                          floating: true,
                          pinned: true,
                          snap: false,
                          primary: true,
                          automaticallyImplyLeading: false,
                          forceElevated: boxIsScrolled,
                          backgroundColor: Colors.white,
                          actions: <Widget>[
                            IconButton(
                              icon: Icon(
                                Icons.close,
                                color: Colors.black,
                              ),
                              onPressed: null,
                            )
                          ],
                          bottom: PreferredSize(
                            preferredSize:
                                Size(MediaQuery.of(context).size.width, 48),
                            child: pinnedHeader(viewModel),
                          ),
                        ),
                      ),
                    )
                  ];
                },
                body: TrackScreen()),
          ),
          floatingActionButton: loadMoreLoader(model: viewModel),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
        );
      },
    );
  }
}

Widget loadMoreLoader({
  TrackViewModel model,
}) {
  return Selector<TrackViewModel, ViewState>(
    selector: (_, store) => model?.state,
    builder: (context, state, __) {
      return Visibility(
          visible: state == ViewState.loadMoreBusy ? true : false,
          child: const CircularProgressIndicator());
    },
  );
}

Widget pinnedHeader(TrackViewModel carInfoViewModel) {
  return Container(
    color: Colors.white,
    child: Container(
      height: 48,
      child: getTabButton(carInfoViewModel),
    ),
  );
}

Widget getTabButton(TrackViewModel viewModel) {
  const Color activeColor = Color(0xFFEF820D);
  return Stack(
    children: <Widget>[
      Align(
        alignment: Alignment.center,
        child: Text(ConstantStrings.appName,
            style: RobotoStyle.subtitle2.copyWith(
              color: kMainColor,
            )),
      ),
      Align(
        alignment: Alignment.bottomCenter,
        child: Container(height: 4, color: activeColor),
      )
    ],
  );
}
