import 'package:base_flutter/core/constant/path_constant.dart';
import 'package:base_flutter/core/constant/strings_constant.dart';
import 'package:base_flutter/core/viewmodel/startup_view_model.dart';
import 'package:base_flutter/ui/view/base_view.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseView<StartUpViewModel>(
        onModelReady: (model) => Future.microtask(
              () => model.handleStartUpLogic(),
            ),
        builder: (context, model, child) => Scaffold(
              backgroundColor: Colors.white,
              body: Center(
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      SizedBox(
                        height: 200,
                        child: Image.asset(
                          ConstantAssetsString.appIcon,
                        ),
                      ),
                      Text(
                        ConstantStrings.appName,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 28),
                      )
                    ],
                  ),
                ),
              ),
            ));
  }
}
