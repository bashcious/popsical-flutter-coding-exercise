import 'package:base_flutter/core/enum/app_environment.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'core/service/flavor_manager.dart';
import 'core/service/locator/locator.dart';
import 'main.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  FlavorManager(
    env: AppEnvironment.dev,
    settings: FlavorSettings(baseUrl: 'https://api.popsical.tv/v3'),
  );

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(
      const PopsicalApp(),
    );
  });
}
